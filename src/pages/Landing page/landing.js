// these are the basic imports of react and is needed in any component.
import React, { Component } from "react";

// these are redux imports and are needed if you plan on using redux.
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { } from "../../actions";

//components
import Jumbroton from '../../components/jumbroton/jumbroton';
import WorkShowcase from '../../components/work showcase/work_showcase';
import KnowledgeContainer from '../../components/knowledge showcase/knowledge_container';
import VerticalSpacer from '../../components/render helpers/vertical_spacer';


class Landing extends Component { 

  render() {
    return (
      <div className="container">
      <VerticalSpacer Amount={2}/>
      <Jumbroton />
      <WorkShowcase />
      <VerticalSpacer Amount={3} />
      <KnowledgeContainer />
      <VerticalSpacer Amount={3} />
      
      </div>
    );
  }
}

//redux shenanigans

function mapStateToProps(state) {
  return {
   
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ 
   }, dispatch); 
  }


export default connect(mapStateToProps, mapDispatchToProps)(Landing);


