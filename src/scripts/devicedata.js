import { Component } from 'react';


//this file can be used to check if the user is on a Desktop or a Mobile.. usefull when you want
//to do complex stuff that are NOT available in CSS

class DeviceData extends Component {

  

    onGetDeviceType() {

        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return false;
            //console.log('mobile');  user is on a mobile device
        }
        else {
            return true;
            //  console.log('desktop');   user is on a Desktop device
        }

    }

}

export default DeviceData;
