import axios from 'axios';
import env from '../web-variables.json';

const ENDPOINT_URL = env.Endpoint_URL;

export function GetWorkShowcase(){
  return  axios.get(`${ENDPOINT_URL}projects.json`)
}
