import React, { Component } from 'react';

// global variables
import env from './web-variables.json';
import './App.css';

//components
import Header from './components/Header/Header_Component';
import Footer from './components/Footer/Footer_Component';
import ParallaxBackground from './components/paralax/container';
import NavigationOverlay from './components/Overlays/navigation';
import SearchOverlay from './components/Overlays/search';

// these are redux imports and are needed if you plan on using redux.
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "./actions";

class App extends Component {
    render() {
        return (
            <section>

                <Header />

                <ParallaxBackground>
                    {this.props.Navigation_Overlay_Open === true ? <NavigationOverlay /> : null}
                    {this.props.Search_Overlay_Open === true ? <SearchOverlay /> : null}

                    {this.props.Navigation_Overlay_Open === false && this.props.Search_Overlay_Open === false
                    ?(<div className='container fadein'>{this.props.children}</div>)
                    :null}
                </ParallaxBackground>
                <br />
                <br />
                <Footer />
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay, UpdateSearchOverlay
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

