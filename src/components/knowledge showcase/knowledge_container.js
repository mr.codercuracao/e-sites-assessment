import React, { Component } from 'react';
// Components
import RightArrowedButton from '../render helpers/button_arrow_right';
import LazyLoad from '../lazy loading/LazyLoad';

import VerticalSpacer from '../render helpers/vertical_spacer';

export default class KnowledgeContainer extends Component {
    render() {
        return (
            <div>
                <h2><span className="accent">/</span> #Knowledge<span className="accent">.</span></h2>
                <VerticalSpacer Amount={2} />
                <div className="container ">
                    <div className="row">
                        <div className=" col-md-6">
                            <LazyLoad debounce={false}>
                                <div className='fadein'>
                                    <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>Online marketing</button> <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>Business</button>
                                    <div><br /></div>
                                    <h3 className="link">Online marketing for businesses. Internal or not?</h3>
                                    <small style={{ color: 'gray' }}>By: Javier van Heckers</small>
                                    <p>An important issue is' can I best show the online marketing work with outsourcing or insourcing?</p>
                                    <div><br /></div>
                                </div>
                            </LazyLoad>
                        </div>

                        <div className=" col-md-6">
                            <LazyLoad debounce={false}>
                                <div className='fadein'>
                                    <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>Recruitment</button>
                                    <div><br /></div>
                                    <h3 className="accent">From a cold shower to a cold bear at the beach.</h3>
                                    <small style={{ color: 'gray' }}>By: Tom Groeneveld</small>
                                    <p>Anyone who thinks that the life of a trainee in Curaçao is only about parties and lying on the beach is wrong.</p>
                                    <div><br /></div>
                                </div>
                            </LazyLoad>
                        </div>

                        <div className=" col-md-6">
                            <LazyLoad debounce={false}>
                                <div className='fadein'>
                                    <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>Social media</button>
                                    <div><br /></div>
                                    <h3 className="link">Is video the future of content creation?</h3>
                                    <small style={{ color: 'gray' }}>By: Javier van Heckers</small>
                                    <p>Do you remember the old days? You were in school and the teacher pushed a large TV on wheels into the classroom.</p>
                                    <div><br /></div>
                                </div>
                            </LazyLoad>
                        </div>



                        <div className=" col-md-6">
                            <LazyLoad debounce={false}>
                                <div className='fadein'>
                                    <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>Social media</button> <button type="button" className="btn btn-light px-3" style={{ backgroundColor: 'rgb(229, 229, 229)', borderRadius: 5 }}>E-commerce</button>
                                    <div><br /></div>
                                    <h3 className="link">Form online back to offline. Really?</h3>
                                    <small style={{ color: 'gray' }}>By: Dinny Pham</small>
                                    <p>Probably you were confused when reading the title: from online to offline? Not the other way around? Not today.</p>
                                    <div><br /></div>
                                </div>
                            </LazyLoad>
                        </div>

                        <div className="col-md-12">
                            <div>
                                <VerticalSpacer Amount={2} />
                            </div>
                            <LazyLoad debounce={false}>
                            <RightArrowedButton className='fadein' Title='More knowledge.' />
                            </LazyLoad>
                        </div>
                    </div>
                </div>

            </div>
        )

    }

}

