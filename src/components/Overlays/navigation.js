import React, { Component } from 'react';

//redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "../../actions";

// components
import VerticalSpacer from '../render helpers/vertical_spacer';

class NavigationOverlay extends Component {

    render() {
        return (
            <div className="fillh " style={{ backgroundColor: 'rgba(0, 0, 0, 0.93)', overflow: 'hidden' }}><div className="container fadein" style={{ color: 'white' }}>
                <div>
                    <VerticalSpacer Amount={3} />
                    
                    </div>
                <div className="row">
                    <div className="col-md-6">
                        <h1 className="link"><span className="accent">/ Home.</span></h1>
                        <h1 className="link"><span className="accent link">/</span> Work<span className="accent">.</span></h1>
                        <h1 className="link"><span className="accent link">/</span> Aproach<span className="accent">.</span></h1>
                        <h1 className="link"><span className="accent link">/</span> Team<span className="accent">.</span></h1>
                        <h1 className="link"><span className="accent link">/</span> Knowledge<span className="accent">.</span></h1>
                    </div>
                    <div className="col-md-6">
                        <div>
                            <h5><span className="accent">/</span> Contact</h5>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">
                                        <p>Kaya Flamboyan 7<br />Willemstad<br />Curaçao</p>
                                    </div>
                                    <div className="col-md-6">
                                        <p>Hamster Drive 1 C-D<br />Cay Hill<br />Curaçao</p>
                                    </div>
                                    <div className="col-md-12">
                                        <p>(+5999) 734 3212<br />info@e-sitescaribbean.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5><span className="accent">/</span> Careers</h5>
                            <div className="container">
                                <p>Are you looking for a new  opportunities?<br /><a className="link" style={{ textDecoration: 'underline' }}>View our vacancies</a></p>
                            </div>
                        </div>
                        <div>
                            <h5><span className="accent">/</span> Follow us</h5>
                            <div className="container">
                                <p>
                                    <a href="https://www.linkedin.com/company/e-sites-caribbean"><i className="fab fa-linkedin-in pr-2   accent link-accent-invert" /></a>
                                    <a href="https://www.facebook.com/esitescaribbean"><i className="fab fa-facebook-f accent px-2 link-accent-invert" /></a>
                                    <a href="https://twitter.com/EsitesCaribbean"><i className="fab fa-twitter accent px-2 link-accent-invert" /></a>
                                    <a href="https://www.instagram.com/esites_caribbean/"><i className="fab fa-instagram accent px-2 link-accent-invert" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div>
                <VerticalSpacer Amount={5} />
                </div>
            </div>
        )
    }

}
function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay, UpdateSearchOverlay
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationOverlay);
