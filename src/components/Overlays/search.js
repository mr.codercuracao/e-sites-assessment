import React, { Component } from 'react';

//redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "../../actions";

import VerticalSpacer from '../render helpers/vertical_spacer';
class SearchOverlay extends Component {

    render() {
        return (
<div className="fillh" style={{backgroundColor: "rgba(0, 0, 0, 0.93)", overflow: "hidden"}}>
            <div className="container fadein" style={{ color: 'white' }}>
                <div>
                <VerticalSpacer Amount={3} />
                </div>
                <div className="input-group input-group-lg mb-3" style={{ width: 'fit-content' }}>
                    <div className="input-group-prepend">
                    <span className="input-group-text accent" id="inputGroup-sizing-sm" style={{ backgroundColor: 'transparent', border: 'unset', fontSize: '5.3vw' }}>/
                    </span>
                    </div>
                    <input type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Type search term here" style={{ backgroundColor: 'transparent', borderTop: 'unset', borderRight: 'unset', borderBottom: '1px solid white', borderLeft: 'unset', borderImage: 'unset', fontSize: '5.3vw', padding: 'unset' }} />
                    <span className="accent" style={{ fontSize: '5.3vw', borderBottom: '1px solid white' }}>.</span>
                    <span style={{ borderBottom: '1px solid white', width: '10vw' }} ></span>
                    <br />
                    </div>


                    <p style={{ color: 'white', paddingLeft: '4.5vw' }}>Hit enter to search or ESC to close
                    <br /><br />
                    <a className="btn btn-outline-default px-3" role="button" style={{ backgroundColor: 'rgb(244, 0, 77)', color: 'white', borderColor: 'rgb(244, 0, 77)' }}>Search.
                       
                       
                        <svg width="20px" height="12px" viewBox="0 0 20 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                <g id="Btn-/-Normal" transform="translate(-198.000000, -20.000000)" fill="#FFFFFF">
                                    <g id="Group-2">
                                        <g id="Icn/Arrow-Red" transform="translate(198.000000, 20.000000)">
                                            <g id="Group" transform="translate(10.000000, 6.000000) rotate(-360.000000) translate(-10.000000, -6.000000) ">
                                                <path d="M15.9487762,5 L12.7163279,1.97856357 C12.697027,1.96052254 12.6785319,1.94163813 12.6608969,1.92196549 C12.3300113,1.5528484 12.3610041,0.985384095 12.7301211,0.654498488 C13.1433709,0.284051314 13.7711275,0.290590848 14.1765702,0.669566592 L20,6.11284719 L14.1585994,11.3468824 C13.7460373,11.7165479 13.1194514,11.7098811 12.7148482,11.3315211 C12.6955873,11.3135095 12.6771311,11.2946563 12.6595335,11.2750166 C12.3293457,10.9065133 12.3604065,10.3401123 12.7289098,10.0099246 L16.0881106,7 L1,7 C0.44771525,7 6.76353751e-17,6.55228475 -3.94430453e-31,6 C-6.76353751e-17,5.44771525 0.44771525,5 1,5 L15.9487762,5 Z" id="Combined-Shape" fillRule="nonzero" />
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                    </p>
                    </div>
                    </div>

        )
    }

}
function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay, UpdateSearchOverlay
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchOverlay);

