import React, { Component } from 'react';

/*
takes in props ->   [
                      Title: string,
                      className: string
                    ]
*/
export default class RightArrowedButton extends Component {

    render() {
        return (
            <a className={`btn btn-outline-default button-accent px-3 ${this.props.className}`} role="button">
                {this.props.Title}
                                    <svg width="20px" height="12px" viewBox="0 0 20 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                        <g id="Btn-/-Normal" transform="translate(-198.000000, -20.000000)" fill="#F4004D">
                            <g id="Group-2">
                                <g id="Icn/Arrow-Red" transform="translate(198.000000, 20.000000)">
                                    <g id="Group" transform="translate(10.000000, 6.000000) rotate(-360.000000) translate(-10.000000, -6.000000) ">
                                        <path d="M15.9487762,5 L12.7163279,1.97856357 C12.697027,1.96052254 12.6785319,1.94163813 12.6608969,1.92196549 C12.3300113,1.5528484 12.3610041,0.985384095 12.7301211,0.654498488 C13.1433709,0.284051314 13.7711275,0.290590848 14.1765702,0.669566592 L20,6.11284719 L14.1585994,11.3468824 C13.7460373,11.7165479 13.1194514,11.7098811 12.7148482,11.3315211 C12.6955873,11.3135095 12.6771311,11.2946563 12.6595335,11.2750166 C12.3293457,10.9065133 12.3604065,10.3401123 12.7289098,10.0099246 L16.0881106,7 L1,7 C0.44771525,7 6.76353751e-17,6.55228475 -3.94430453e-31,6 C-6.76353751e-17,5.44771525 0.44771525,5 1,5 L15.9487762,5 Z" id="Combined-Shape" fillRule="nonzero" />
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </a>
        )
    }

}

