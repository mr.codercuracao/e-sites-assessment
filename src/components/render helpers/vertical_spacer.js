import React, { Component } from 'react';

/*
takes in props ->   [
                      Amount: number,
                      className: string
                    ]
*/
export default class VerticalSpacer extends Component {


    renderBreaks = () => {
        let Amount = this.props.Amount ? this.props.Amount : 1;
        let Elements = [];
        for (let x = 0; x < Amount; x++) {
            Elements.push(<br  key={x}/>)
        }
        return Elements
    }

    render() {
        return (
            <div className={`${this.props.className}`}>
                {this.renderBreaks()}
            </div>
        )
    }

}

