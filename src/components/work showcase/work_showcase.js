import React, { Component } from 'react';
import { GetWorkShowcase } from '../../API ENDPOINTS/endpoints';
import { PulseLoader } from 'react-spinners'; // used for loading

import LazyLoad from '../lazy loading/LazyLoad';

// Components
import RightArrowedButton from '../render helpers/button_arrow_right';
export default class WorkShowcase extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: []
        }
    }
    componentWillMount() {
        GetWorkShowcase().then((res) => {
            /*
                A tip for the user whp created this endpoint:
                1. consider putting a size k/v pair ranging from 1 to 12 on each element you send..
                this allows the front-end to know how much screen realestate an item wants or should take.
            */
            if (res.data) {
                this.setState({
                    loading: false,
                    data: res.data.data
                })
            }
        })
    }
    render() {
        if (this.state.Loading === true) {
            return (
                <div className='sweet-loading'>
                    <PulseLoader
                        color={'#00FFFF'}
                        loading={this.state.loading}
                    />
                </div>
            )
        } else {
            if (this.state.data.length < 4) {
                return <h1>This component requires atleast 4 showcase items to function</h1>
            } else {
                return (
                    <div>
                        <br />
                        <div className="jumbotron" style={{ paddingBottom: 'unset', paddingTop: 'unset', backgroundColor: 'transparent' }}>
                            <h2><span className="accent">/</span> Work<span className="accent">.</span></h2>
                            <br />
                        </div>
                        <div>
                       
                            <div className="row">
                                <div className="p-3 col-md-8">
                                <LazyLoad onContentVisible={() => console.log('content visibile')} debounce={false}>
                                    <div className="card  rounded fadein" style={{ border: 'unset' }}>
                                            <div>
                                                <img alt={"404"} className="card-img fadein" src={this.state.data[0].image} style={{ width: '100%' }} />
                                            </div>
                                        <div className="card-img-overlay" style={{ top: 'unset' }}>
                                            <h5 className="card-title"><img alt={"404"} src={this.state.data[0].logo} style={{ width: '6rem', height: 'auto' }} /></h5>
                                            <p className="card-text" style={{ color: `${this.state.data[0].textColor}`, width: '50rem', height: 'auto' }}>{this.state.data[0].title}</p>
                                        </div>
                                    </div>
                                    </LazyLoad>
                                </div>
                                <div className="p-3 col-md-4">
                                <LazyLoad onContentVisible={() => console.log('content visibile')} debounce={false}>
                                    <div className="card  rounded fadein" style={{ border: 'unset', overflow: 'hidden', height: '100%', zIndex: 1 }}>
                                        <div>
                                                <div>
                                                    <img alt={"404"} src={this.state.data[1].image} style={{ position: 'absolute', width: '25rem', height: 'auto', 
                                                    zIndex: 2,
                                                 }} />
                                                </div>
                                        </div>
                                        
                                            <div>
                                                <img alt={"404"} className="card-img fadein" src={this.state.data[1].image} style={{ zIndex: 3 }} />
                                            </div>
                                        <div className="card-img-overlay" style={{ top: 'unset', zIndex: 4 }}>
                                            <h5 className="card-title"><img alt={"404"} src={this.state.data[1].logo} style={{ width: '6rem', height: 'auto' }} /></h5>
                                            <p className="card-text" style={{ color: `${this.state.data[1].textColor}` }}>{this.state.data[1].title}</p>
                                        </div>
                                    </div>
                                    </LazyLoad>
                                    </div>
                                <div className="p-3 col-md-6">
                                <LazyLoad onContentVisible={() => console.log('content visibile')} debounce={false}>
                                    <div className="card  rounded fadein" style={{ border: 'unset' }}>
                                            <div>
                                                <LazyLoad><img alt={"404"} className="card-img fadein" src={this.state.data[2].image} /></LazyLoad>
                                            </div>
                                        <div className="card-img-overlay" style={{ top: 'unset' }}>
                                            <h5 className="card-title"><img alt={"404"} src={this.state.data[2].logo} style={{ width: '6rem', height: 'auto' }} /></h5>
                                            <p className="card-text" style={{ color: `${this.state.data[2].textColor}` }}>{this.state.data[2].title}</p>
                                        </div>
                                    </div>
                                    </LazyLoad>
                                    </div>
                                <div className="p-3 col-md-6">
                                <LazyLoad onContentVisible={() => console.log('content visibile')} debounce={false}>
                                    <div className="card  rounded fadein" style={{ border: 'unset' }}>
                                            <div>
                                                <LazyLoad><img alt={"404"} className="card-img fadein" src={this.state.data[3].image} /></LazyLoad>
                                            </div>
                                        <div className="card-img-overlay" style={{ top: 'unset' }}>
                                            <h5 className="card-title"><img alt={"404"} src={this.state.data[3].logo} style={{ width: '6rem', height: 'auto' }} /></h5>
                                            <p className="card-text" style={{ color: `${this.state.data[3].textColor}` }}>{this.state.data[3].title}</p>
                                        </div>
                                    </div>
                                    </LazyLoad>
                                </div>
                            </div>
                            
                        </div>

                        <div className="jumbotron" style={{ backgroundColor: 'transparent' }}>
                            <div className="container px-6">
                            <LazyLoad debounce={false}>
                                <RightArrowedButton Title='View more work.' className='fadein' />
                                </LazyLoad>
                            </div>
                        </div>
                    </div>
                )
            }
        }

    }

}

