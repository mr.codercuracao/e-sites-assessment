import React, { Component } from 'react';
import RightArrowedButton from '../render helpers/button_arrow_right';
/*
takes in props ->   [
                      className: string
                    ]
*/
export default class Jumbroton extends Component {
    render() {
        return (

            <div className={`jumbotron ${this.props.className}`} style={{ backgroundColor: 'transparent' }}>
                <h1>We craft next level<br /><span className="accent">websites</span>, campaigns <br />&amp; mobile apps<span className="accent">.</span></h1>
                <div><br /></div>
                <RightArrowedButton Title='Meet our people.' className='none' />
                <div><br /><br /></div>
            </div>)
    }

}

