import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "../../actions";

import LazyLoad from '../lazy loading/LazyLoad';
class Footer extends Component {
    jumpToTop(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }
    render() {
        return (
            
            <div className='fadein' style={{width: '100%', backgroundColor: 'white', color: 'black', zIndex: 9999,
            display: this.props.Navigation_Overlay_Open === true || this.props.Search_Overlay_Open === true ? 'none' : ''
            }}>
            <LazyLoad debounce={false}>
<footer className="container">
   <hr />
   <p className="inline">
      <svg className="align-bottom" xmlns="http://www.w3.org/2000/svg" width="27px" height="32px" viewBox="0 0 27 32" version="1.1">
         <defs />
         <g id="Footer" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd" transform="translate(-55.000000, -1049.000000)">
            <path d="M68.5000405,1059.85719 L68.4996356,1065.2857 L63.9995681,1068 L59.4995816,1065.2857 L59.4995816,1059.85711 L55,1057.14255 L59.4995816,1054.42834 L63.9996491,1051.70744 L68.4996356,1049 L81.9995951,1057.14264 L82,1062.57149 L72.9996221,1068 L68.4996356,1065.2857 L72.9996221,1062.5714 L72.9996221,1057.14264 L68.4996356,1054.4286 L63.9996491,1057.14264 L68.5000405,1059.85719 Z M72.7997517,1073.20021 L82,1068 L82,1073.19997 L68.2,1081 L59,1075.79987 L59,1070.5999 L63.5998344,1068.00024 L68.1998344,1070.5999 L68.1999172,1075.80011 L72.7997517,1073.20021 Z" id="Combined-Shape" fill="#F4004D" />
         </g>
      </svg>
      <span className="align-baseline px-3">© 2015  E-sites Caribbean</span>
      <span onClick={this.jumpToTop} className="float-right align-baseline ">Back to top <i className="fas fa-angle-up accent" /></span>
   </p>
   <br />
</footer>
</LazyLoad>
</div>
        );
    }
}

function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay, UpdateSearchOverlay
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
