import React, { Component } from 'react';

//redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "../../actions";

//css
import './header.css';

import Headroom from 'react-headroom';

class Header extends Component {

  onRequest_UpdateNavigationOverlay=()=>{
    this.props.UpdateSearchOverlay(false);
    this.props.UpdateNavigationOverlay(!this.props.Navigation_Overlay_Open);
  }
  onRequest_SearchNavigationOverlay=()=>{
    this.props.UpdateNavigationOverlay(false);
    this.props.UpdateSearchOverlay(!this.props.Search_Overlay_Open)
  }
  onRequest_CloseAll=()=>{
    this.props.UpdateNavigationOverlay(false);
    this.props.UpdateSearchOverlay(false)
  }
  render() {
    return (
      <Headroom style={{zIndex:'1000'}}>
        <div>
          <nav  className={` navbar ${this.props.Search_Overlay_Open === true || this.props.Navigation_Overlay_Open === true ? 'navbar-dark' : 'navbar'}`}>
            <a className="navbar-brand" >
              <img src="data:image/png;base64,UklGRvQRAABXRUJQVlA4TOgRAAAvo0AoEP8HuY0kR1JUz8nfOnqW3vs8kd2VlXnCCciRJElSvGq5X6cACIgUKL0LdFVCiiTJkRTVq39L9HAdmHsfFlUp4Fbbtjy5k+//4+7uQukVa8AELMMGVFpSMQOH2h0qd//dkiQA+P6TAiZgAiZgAmTQA9mkXUuYJQIEeExqAAN4wKiaX4qzqnEoBJCBAD7wCf4IOoVO0MBfUonivozuJOVNvDK3YDtpzhulevZXn3j1GPSUJVgQI0bCCBNzCDESxCimmWHEHKJjoohpIYKYgGY6oolmDtExTYhppgnFjDDCYUQcpiMsjNBMEzvLtjTzbTlZNnEsShBxiGbEJOXdZ5LmoIyRVJt1fI7N6Ls3MZblGUsTr7PcTFw/v/+aJ3r/r6Mjvf/3i2Lt/P9Le/9c/pB0JFadRR1lJt7t3w3O2E0qnMip6gwjsPL88vKXid9e3+Hb53v16f7pu/oiXr++7DffE37ws4yrJg5CSRf6f4+T/ukoMWM3qQfiqTqVmfZG8ebFnfe+rlc1fR39J1WchOt+/nVVKemXRjIhNmMJvIBR0oST0HW5dPbt7zqpPetUtnJ0lK0MFAqLxvFJypP/aD3bZ2+cpFEQrZ7kHIsZK0nHIA1iSV+jwpuYN4JwVzXqjCNJZRp/tslsBRDu/PgvxRl70X+CSPqStO7kjTA14xGwwFfVaDMOUkQJURD05gNMgBaVCBJFJqqzBKOjl++P8erjAOXx+RFZjrf6jKsmtvo0Vp/6dbnsUYwJ+nW5hO/ursHVx1C09PXwsyMdA2nbLPXv+hcREROwa9gepVY1mCjt/5+2kZaZadBDy0ynfQV73D3vkeHYuZaep5PsMHY6zMwzr6w80NkMSH/JkuzCUeBEtmRLEf2XIEmS3DY5gDnYSxexA4Z/ADuSJNWRB+eBfGBmZmZm6a3/Juhp1dP7DDh4q8+LiP47cCS3bfbSeyXFI/0Bv9q2KZJk29rVg8c4Akx98DiDoY+DGBIz85B6iKQxMxYzM1NiMTNXidXagO97zdzDM7JkdzdzQzeziP5LEMA2bhuQbBAQkrpLcpJ6D3giw3hJeBQZxhLek7abcVi7AFi2oafmMytk/r8frkq6QVoh0+189eJraxIQpdHzUZcpRlFoery6LqFEfFXTo0DBdv/OO1+8n6REZecVhl/J3kYzxO6PViap4Y3VDNE0DL+keWJLjG99nTCFkjHG/Eufm/oreQGGfm99spqJ1np9E4lI/SWlT82NUZoYIyWLn31CtDA2OEVJcFBvjLzSqQZR7HpjRdLtch1lZRwKAvtLvvqIPxvse977t9fiWZxJtzbee9xFebvlp4t9PhjrYbzSS0DwJ73awwB/SPbpmd6iexvquo7W0t+HCq6wCFIXtAZHXbZp0m7aHHCOGIti5+trEqFR9mSBguzikMHjVNobyznYt7mWUr67LhEe0Ukp67oo4PS0t1vKA1nGcPZcKaVEBTeTqKyUsiyLgpvwGxTsbcS6cP7LVfUvKrgZhTe2qiowHS5L7IUMt61QMe5xDuovSjBf7jKo4GYYShqzE0wviwLa49yWKBDbiEh/a8nKGFRwMw7dGFNJ+pstsg3gN0oCGd48QF9lzBAuuJmHdioYU4GuAgW4IUiFSBpyzlHf/sbsfnNFkosoG2sM9YZwzhE3LbcnrSUzYgTRAuebYAwquLmJ2uAfBE4HDXAANbHWiuAcuQcT2ecbklxFryAScq92jp+1KdtIdGBWXeOWYIIxGz9eleQsQm8j8SNVOPEOLBOpo6zltA/mUko4YjcquDmMsmfhACklTBWnlAxsxHgdE0wwA7b1jrCC+734smz1gAU/HF4GbNMpCS+85og3AZ/LPkgExXc//fnLD8KTehk+ghnxeEk+T1r6MGJNCbJWn7iC++Ov/xTFXz8LH+BNXwsk4GvSRhFNzBzOpRx5aMLA7tYXLeFmd8qtS5Oflo22A2GXH8FLOudSMLTB7RieFcqsFVdw4zNHaXTIG0jL1qLkZXhEMIpoYgQKhpTGhND6QljBDXVvSi9nkeCn5S9aqNOgOoI7uYA2nD+H8/Dvp8IKbvmk3uveC9PTcmNDAAqiCUrrHy0QPKeu5YgxQVzBTW8VpnPVQFpGLx4t5pwC3QXsTqNJHuXCCm50/FCwthl+Wg7BeJJmRbTh19ZAIkawdVUSOZMoe2db+AU28ZLeh4GWGIWDkurWCXJpDQTvJSPKKkDtAzo7IiQWiqFjkUoHHcFvxmiLpFPu+bo2oWX1sICC278rklZJIkpbPWTq+vnBfelS+xE9zK92ViSR07JSGqF2s4LX/DI7+GPwQV2aPMAlt7qe9Phsb+ISmzp4/Ghe0BP7HIfQxoevN9fMyD35Ie5XPIKdGxyH08+V6xC9gTOPU1C428O9mU1E5gDdGQ4cikf01Kli3UquFah7L0QgDHc0MIcHQ7RdmXhEqGv8IDjMgvHGOR8fA6XAZOJhAe7MTFq71SYm0h8Q72coJijLbQ7VttDqoNHhPyaSPyLx7/wYR0WUIbRR0xCqcY6fYpPdo6euTXJY/GsLURzg4+Ac2pR9a6CDTxaxGfyVjNDUkPNhhSXgo2P1iK/7HXwwZNfGGD8qwamh5SnaXGIkVh0Yvt8g7B85QR3y3qFplCUWjZGDbjfNvbLk+OiO3nmR8Aj37jdtLcAMBfPOvIQjcz/sHXr8OkW7CeXd/7meqI8KrpzMowy6bHcKUUouNtiGK5NdxjilGA+R5p4tFR9rJ/g62jiqtLZBPIf+hUU/i4i25cnNk7i6IXfN7PxUGYPgCldvnkQbc2SxGWhj1XxVYW2g5qBnCPP1gtDT/O5NzW/R8ppEppvH7k7MXxMm2mYaqTk8BGw+lE5wEfBvhVjAc0PXORiS2sCmTldWTFV4YhSmylpkBNcys1jACql4DeZIC0/RWWupDKZTCugZwniG8gd4qdOwco0TTA9QisvE7Fh2EotvdMk4CFsGY9UEp7lJYL+NdzO0wU28ZTJgWZgpa4HJOTZ17Q1WVbPjvOLzb4x1GuVYsJZE97EKM76u+Y1xsza3B21Y7My1HQzw8GIc2m+PYgcT/mheIDvbdRPEbaChsKP77SEu/vS0YSV3dJypO+K+PNA718YA0+b0Qx7s/+DO2PviRJM79O6XiQMhxrjprTm0TZXavLiIfDyz0eJGJBzCjowdPiiMDXbOfTDilo9PN5aOKseOYF3CjTjqewHb/LB7P8ll2AMchY4RxHzRTxFC4q9I0FAUTlQJWSdZ1M++bxZ8Akf88VILEerdRb0f2O+Tog8fP+IBfORB5STs/YC+qdc4lcX/lu9z8hEFOaf4XSdEIaJjB+39yEYUeETCdQZn6M5pmennUTCTsGjoznC5cLy5lDUgI9O4dK53gd/l7nmsyA1NVcXkAnb+0GUBmtx4qaq4NNxM/riqKmMYRhehKxcwlxVkRIHLJaxQkM2v9yMKdE7RsV/IzJF6gIwDiS/yy3Qv9dI1XopwE7l11ZVZMd2Tvnbp6i1EE/YxUoTfLi8Ivf1GUAiqNqLaQ1jVb1pWiGckmsHYryIkFSfG35YP0lmTJvC3RvCrdlW3vF+kMztNfem6v/+qBt42EHBj3lwAlJNetX6sLhzTs09UgFJgW2a8rcvg3wBNWJk/Q8ZOQK6Fn+r9zsRd/yT8fdU4MqbiZ/F0dWCjGMdVLcHuWmuc+6muRiRkNu74P3QzP9P4man7mcbVesMOdmZCrIbsSoHBD1s+NNNEfGTafjY0K3pkSK4SamNnS9jxf7x4TzP3l3z04wh6E4+7/Ul4oehx7P++czLEXXvU5p38dPfBLWte/eLMZDHJxytaE49J96rbknfz852b/7zHxonR9W3YsGWb2/79D840EamNJ2a+Y5vW+C0bwo6+qwtrf8rJZpV4cL+JU63Z+tdvNvPj86UTNnnCHZwf/2rWKCpwl3+nP7ObS1ubZpagcqan/Y42MLanF1959/Tfg9DHtD0xwo52PT2n0YTZCfBPhuqhvWGb35r2HvrBF2aaiJjJbTmyO7Q3Ge83QL9XDFsDYpuPjz1p32ZI8Gzl7L/34YbviWmRN4j5Xs9lyCRU2bcnWXqO/Sro4sBMbcYYwkHOjvKP3357Q28YvkzmMXn7t/9I1XEd3BA2MaifbGT8VAatJXCOkh44+xao356YvPbVpt543aS4l5aW2fbkgbfruYrmck6CJwq6IsMqkMJB9iTCsfKjz0/fvW7RHCvcZVMOoK2SYZ0csjZh1uyy6c/7aIID/cj0oD+yFejh4bsfmpmqyP7wANy4bWRrqaJS/9a0J9tZQ4+o1QBIu6ziIUqgRznTIcpjf/zWFN1+mct4jKpPXEcFrnjMn60FrRaslUhxl9qzuZQDEZPICYP01Gtfnha4dkRuDDGsGEspSdm1Eu0VWxt/4etXpkKT84g7cRJz/P4HpwGxHbG8cY4Ku8DhetVUgt4kobg98xRzIxqOOO89kI3fWfbbqp3xwE56f8ShY5zjOYpOScJlWppBpOI6SuY6cgLE/lg5/YdvLOvt6b3/Q1vKIVCNk9mUsJCEv20iwEsqgRbKOLawGBHvF5cOnz126vR/vrR8+JJtxxIPL2nviY05sO0A1Wbk+OKpBqQDRMnNXt7FqRWLIxAQF4lwtj9Zpv8uoUstFzFNqsBGVtU+Lnt5dr+oCSPqgFMDgCZ0zY0CZaLPUp4/vfHbb28eWvm2o4qLzAS+aQ5tCTjgp2yqjYrJjIm7LFFNrhDjmbZdAPGkWSQxXjh/+hdfbRolt92FGC0xD+rCjlYdiHNk26c1qWtnjXS2RuAMDtBMCartuizQJUvorvkffb7ZA9f7jmCtwPNC1ypC3MkAPru/DiYhmom7XCKLVwelmKj5Lp8bR9pYWLxAwvHvNXYs59STMrDqjRnHcwcX5pVSrFwdWF1bioPrlyo2KSpcil3h/0jdOd6iL11Z1IK4sZljORcvqFr3l3gzPreQkgpsUxW2GphAsD9FEYggDgdCiEy6TFNe6nlQPN9d900cy671Y9fFaC0xmjMvMONfx1VBRRnZWjxJaxEC83LhNYaBlw2C6+IJc4nyxoWB0v94wmM5+mt+JFtLdckYJ9iIRbLxIjHCkwV31+sCb38iQeFRRwSmyRfdpbioidzT1/3x705wLDvfXxu1JraoY7zocu4SsEcdKiLDP+VTjtonhLVoEBIHdYKIeaVf4hxH72sfy73/A6kjl73SYwkqlxYKj4DjMyCsiRsL0kFC7ySifd+2Aql3LH/JCqxt+554ShJ+lTluI/AeTSUikbqKRLyFIpCb6n6+w67+bnvjApLqY7lLyIb2bsvM3clWKbChaiRqxMOomWCSLdoYcAG5Dcni83jlCYqa8rj8WNaq9zQnqq6Pz1mFus0MbcYImzELsUMD2p1zsMYtgQQWLQ1igQwtkXGUHcsljyOxlhys0CjLahCYgUX2O4d+lzI2Uw4GSae8ef9pBNcTBqT/IRzLsfdcAAe2p9Eq6ZwGanJk8GaTTHPrIQi3w+ObigReAw1g1d/7UE6aveDARUhR6nEIoDw0knlq/NXKmOQokbg45yHT82eX8hMmIGz0WrfIXI65T1DzXKJNPCwaRUNHSQhS8nBAMSj1AEj/LOcrTyhaCEfR55yfAwtK3UZ1eChl/GuR9nKjAHgNMwiipkQDEAiRoYNKC+pgzK4ibHRTBa21jITXZ8nG8hAokhDB0D+TRg+OGJCFTlQLq6/P8MuQKLkgEOmgGuQ+C6mXRGLFSljlKIFNIyxDfu1wC6YUBFiJIsHYCH+3BAXmNPJrAig5ctjFBNNkSWpFAsV9ikChiEkqGxgFrEYvh+SuAglEEMQQFFHNRrgmqIlcduGaWSQsYAusQgShB4qEvT7nyk1k6iGuKaZAgqCETizxPGsEOdxa8hu6zsC/Wt5c8gBDhRjAm4214Ddv6LrXV28iKwSUssOEiRgikx0VVPWR+6VD6CjeqEuBljUwHcxnLPRK0bqjCKFy7ulHxT8IyFmDgixQMQYq55qbyMoxZAq0a10qQmv055xzHccKM8hbkqjooWOlQ9aShLxXAytSsFYgNUL0ypUVDFlUeGtiZbaqVqn8PansvTTx0piu7nwvOyySoQE=" width="30" height="30" className="d-inline-block align-top" alt="" />
              <span style={{ color: this.props.Search_Overlay_Open === true || this.props.Navigation_Overlay_Open === true ? 'white':'black' }}>E-sites <span className="accent">/</span> Caribbean<span className="accent">.</span></span>
            </a>
            <ul className="navbar-nav mr-2">
              <li className="nav-item"><span>
                <i onClick={this.onRequest_SearchNavigationOverlay} className="fas fa-search accent pr-3" style={{ fontSize: "1.25rem" }}></i>
                
                {this.props.Search_Overlay_Open === false 
                ?<a><i onClick={this.onRequest_UpdateNavigationOverlay} className="fas fa-bars accent" style={{ fontSize: "1.25rem" }}></i></a>
                :<a><i onClick={this.onRequest_CloseAll} className="fas fa-times accent" style={{ fontSize: "1.25rem" }}></i></a>
                }
                
                
                </span>
                </li>
            </ul>
           
          </nav>
        </div>

      </Headroom>);
  }
}

function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay: UpdateNavigationOverlay,
         UpdateSearchOverlay: UpdateSearchOverlay
    }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Header);