import React, { Component } from 'react';
import { Background, Parallax } from 'react-parallax';

// Redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UpdateNavigationOverlay, UpdateSearchOverlay } from "../../actions";
class ParallaxBackground extends Component {
    render() {
        return (

            <Parallax
                blur={{ min: 3, max: 5 }}
                strength={450}
            >
                <Background className="custom-bg">
                    <img src={require('./darksymbol.png')} alt="fill murray" style={{ 
                        position: 'absolute', top: '15rem', bottom: 0, left: '-12rem', width: '60rem', height: 'auto',
                        filter: this.props.Navigation_Overlay_Open === true || this.props.Search_Overlay_Open === true ?'invert(100%)' :''
                }} />
                </Background>
                {this.props.children}
            </Parallax>);
    }

}

function mapStateToProps(state) {
    return {
        Navigation_Overlay_Open: state.NavigationOverlayReducer,
        Search_Overlay_Open: state.SearchOverlayReducer
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        UpdateNavigationOverlay, UpdateSearchOverlay
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ParallaxBackground);



