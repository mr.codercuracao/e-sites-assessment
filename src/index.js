//base react imports
import React from 'react';
import ReactDOM from 'react-dom';

// importing the  layout file
import App from './App';

// importing the service worker
import registerServiceWorker from './registerServiceWorker';

// importing Thins that are necesary to create the react-router
import { Router, Route, IndexRoute, browserHistory } from "react-router";

//redux
import reducers from './reducers';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxPromise from 'redux-promise';

import './App.css';

//Dynamic importing of routes
import DynamicImport from './components/Dynamic Importing /RouteImporter';


const Landing = (props) => (
  <DynamicImport load={() => import('./pages/Landing page/landing')}>
    {(Component) => Component === null
      ? <p>loading...</p>
      : <Component {...props} />}
  </DynamicImport>
)
const NotFound = (props) => (
  <DynamicImport load={() => import('./pages/Error pages/not-found')}>
    {(Component) => Component === null
      ? <p>loading...</p>
      : <Component {...props} />}
  </DynamicImport>
)

// create a redux STORE with middleware
const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);


const app = document.getElementById('root'); // what HTML element this file will render in

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={browserHistory}>

      <Route path="/" component={App}>

        <IndexRoute component={Landing}></IndexRoute>


        <Route path='*' exact={true} component={NotFound} />
      </Route>
    </Router>
  </Provider>,
  app);



registerServiceWorker();
