import { SEARCH_OVERLAY } from "../actions";

export default function(state = false, action) {
  switch (action.type) {

    case SEARCH_OVERLAY:  
    {
        return action.payload; 
    }
    default:
      return state;
  }
}
