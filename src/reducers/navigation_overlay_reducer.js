import { NAVIGATION_OVERLAY } from "../actions";

export default function(state = false, action) {
  switch (action.type) {

    case NAVIGATION_OVERLAY:  
    {
      console.log('received navigation overlay data in reducer')
        return action.payload; 
    }
    default:
      return state;
  }
}
