import { combineReducers } from "redux";
import NavigationOverlayReducer from './navigation_overlay_reducer';
import SearchOverlayReducer from './search_overlay_reducer';

const rootReducer = combineReducers({
  NavigationOverlayReducer,
  SearchOverlayReducer
});

export default rootReducer; 