export const TEST = "test"; 
export const NAVIGATION_OVERLAY = "navigation_overlay";
export const SEARCH_OVERLAY = "search_overlay";

export function UpdateNavigationOverlay(bool = true) {
  console.log('updating navigation overlay state')
  return {
   type: NAVIGATION_OVERLAY,
   payload: bool
  }
}
export function UpdateSearchOverlay(bool = true) {
  return {
   type: SEARCH_OVERLAY,
   payload: bool
  }
}