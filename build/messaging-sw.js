console.log("Service Worker Loaded...");

self.addEventListener("push", e => {
  const data = e.data.json();
  console.log("Push Recieved...");
  self.registration.showNotification(data.title, {
    body: "Notified by a kid with a computer",
    icon: "http://i0.kym-cdn.com/entries/icons/facebook/000/007/559/headache_puppy.jpg"
  });
});