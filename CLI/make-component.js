#! /usr/bin/env node
var shell = require("shelljs");
var fs = require("fs");

//shell.exec("cd ./src && mkdir components"); // cd into src & makes a dir

if (fs.existsSync('./src/components/NewComponent.js')) {
    console.error('looks like you already have a NewComponent.js file inside your folder.. try renaming this file to a different name or moving it before running this command again');
}else{
    shell.exec("cd ./src/components && touch NewComponent.js"); // make component file



var ComponentString = `
import React, { Component } from "react";

export default class RenameMePlease extends Component { // rename this

  constructor(props) {
    super(props)
    this.state = {
      Name: 'Duart'
    }
  }
  
  /* this is a very simple component that demonstrate the use of 'props' to pass data from
  a Parent component over to a child component (this one you are viewing right here)
  */
  render() {
    return (
      <div>

           {this.props.Title} 
      </div>
    );
  }
}

/* dont forget that this file does NOT include the redux stuff..  */

/*
function mapStateToProps(state) {
  // Whatever is returned will show up as props
  return {
    TestReducer: state.TestReducer
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ TestReducer: Test }, dispatch); // here the valye 'Test' is the function we imported from our ./actions/index.js directory/file 
}

export default connect(mapStateToProps, mapDispatchToProps)(RenameMePlease); 



*/



`

shell.exec("echo '"+ComponentString+"' | cat - ./src/components/NewComponent.js > temp && mv temp ./src/components/NewComponent.js");
console.log("echo Your Component has been made.. you can find it under `./src/components` with the name NewComponent.js .. do NOT forget to rename this component and also give the Class a PROPPER NAME!!!!");
}
