# This project was built using reactjs

# To test this project 
2. use any http-server you might have e.g `npm install -g serve`
4. serve the "_build_" folder

# Notes
> this project was built using [react](https://reactjs.org/)

# Levels
| level         | status        |
| ------------- |:-------------:|
| 1             | completed     |
| 2             | completed     |
| 3             | completed     |
| 4             | completed     |

## level 4 details
here are some of the things i worked on for level 4:
1. dynamic route loading (there's only 2 routes, the error page and the landing page)
2. Redux (to **show redux in action** i made the overlays appear via redux)
3. Easy to use lazy loading that wraps over any html element (jsx supported)
4. paralax (background logo)
5. slight styling changes in some places
6. full PWA / SEO set-up
7. notification support (no back-end to handle this in this example but it will ask you for permissions)

# performance
## Test 1 initial load ( caching happens here )
### Details: 
* URL: http://localhost:5000/
* Fetch time: Oct 16, 2018, 1:44 PM AST
* Device: No emulation
* **_Network throttling: 562.5 ms HTTP RTT, 1,474.6 Kbps down, 675 Kbps up (DevTools)_**
* **_CPU throttling_** : **_4x slowdown (DevTools)_**
* User agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100
Safari/537.36

### Results:
![404](https://gitlab.com/mr.codercuracao/e-sites-assessment/raw/master/perf.png)

## Test 2 second+ loads ( cached content PWA kicks in )
### Details: 
* URL: http://localhost:5000/
* Fetch time: Oct 16, 2018, 4:32 PM AST
* Device: No emulation
* **_Network throttling: 562.5 ms HTTP RTT, 1,474.6 Kbps down, 675 Kbps up (DevTools)_**
* **_CPU throttling: 4x slowdown (DevTools)_**
* User agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36

### Results:
![404](https://gitlab.com/mr.codercuracao/e-sites-assessment/raw/master/perf2.png)